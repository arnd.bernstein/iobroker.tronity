/*global systemDictionary:true */
'use strict';

systemDictionary = {
    'Adapter settings for Tronity': {
        'en': 'Adapter settings for Tronity',
        'de': 'Adaptereinstellungen für Tronity',
    },
    'Description': {
        'en': 'To retrieve data, client_id and client_secret must be entered from the Tronity Platform. Then check the values and select the vehicle.',
        'de': 'Um Daten abzurufen müssen client_id und client_secret von der Tronity Platform eingegeben werden. Danach die Werte prüfen und das Fahrzeug auswählen.'
    },
    'Check Login Data': {
        'en': 'Check Login Data',
        'de': 'Login-Daten prüfen'
    },
    'Select your Car': {
        'en': 'Select your Car',
        'de': 'Wählen Sie Ihr Fahrzeug aus'
    }
};