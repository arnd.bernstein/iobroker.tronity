/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/no-this-alias */
import * as cache from 'memory-cache';
import * as tronity from 'tronity-platform';
import * as utils from '@iobroker/adapter-core';

declare global {
    // eslint-disable-next-line @typescript-eslint/no-namespace
    namespace ioBroker {
        interface AdapterConfig {
            client_id: string;
            client_secret: string;
            vehicle_id: string;
        }
    }
}

class Tronity extends utils.Adapter {
    private GetAllInfoTimeout: any;

    public constructor(options: any = {}) {
        super({ ...options, name: 'tronity' });
        this.on('ready', this.onReady.bind(this));
        this.on('message', this.onMessage.bind(this));
        this.on('stateChange', this.onStateChange.bind(this));
        this.on('unload', this.onUnload.bind(this));

        // Timeouts and intervals
        this.GetAllInfoTimeout = null;
    }

    /**
     * Is called when databases are connected and adapter received configuration.
     */
    private async onReady(): Promise<void> {
        this.log.debug('Starting Tronity');
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const Adapter = this;
        this.subscribeStates('command.*');
        await Adapter.setStateAsync('info.connection', false, true);

        if (this.config.client_id && this.config.client_secret && this.config.vehicle_id) {
            await Adapter.setStateAsync('info.connection', true, true);

            await Adapter.initSetObject('command.Charging', 'boolean', 'data');
            await Adapter.initSetObject('level', 'number', 'data');
            await Adapter.initSetObject('usableLevel', 'number', 'data');
            await Adapter.initSetObject('range', 'number', 'data');
            await Adapter.initSetObject('speed', 'number', 'data');
            await Adapter.initSetObject('power', 'number', 'data');
            await Adapter.initSetObject('chargerPower', 'number', 'data');
            await Adapter.initSetObject('phases', 'number', 'data');
            await Adapter.initSetObject('voltage', 'number', 'data');
            await Adapter.initSetObject('current', 'number', 'data');
            await Adapter.initSetObject('energyAdded', 'number', 'data');
            await Adapter.initSetObject('milesAdded', 'number', 'data');
            await Adapter.initSetObject('socMax', 'number', 'data');
            await Adapter.initSetObject('consumption', 'number', 'data');
            await Adapter.initSetObject('outTemp', 'number', 'data');
            await Adapter.initSetObject('inTemp', 'number', 'data');
            await Adapter.initSetObject('passTemp', 'number', 'data');
            await Adapter.initSetObject('limitSoc', 'number', 'data');
            await Adapter.initSetObject('fan', 'number', 'data');
            await Adapter.initSetObject('climate', 'boolean', 'data');
            await Adapter.initSetObject('seatHeating', 'number', 'data');
            await Adapter.initSetObject('preconditioning', 'boolean', 'data');
            await Adapter.initSetObject('charging', 'string', 'data');
            await Adapter.initSetObject('driving', 'boolean', 'data');
            await Adapter.initSetObject('doorLocked', 'boolean', 'data');
            await Adapter.initSetObject('sentryMode', 'boolean', 'data');
            await Adapter.initSetObject('windowsLocked', 'boolean', 'data');
            await Adapter.initSetObject('odometer', 'number', 'data');
            await Adapter.initSetObject('latitude', 'string', 'data');
            await Adapter.initSetObject('longitude', 'string', 'data');
            await Adapter.setStateAsync('command.Charging', false, true);

            await Adapter.GetAllInfoTask();
        }
    }

    private async initSetObject(name: string, type: any, role: string) {
        await this.setObjectNotExistsAsync(name, {
            type: 'state',
            common: {
                name,
                type,
                role,
                write: true,
                read: true
            },
            native: {}
        });
    }

    private async getToken() {
        const Adapter = this;
        try {
            if (cache.get(Adapter.config.client_id)) {
                return cache.get(Adapter.config.client_id);
            }

            const token = await new tronity.AuthenticationApi().authControllerAuthentication({
                client_id: Adapter.config.client_id,
                client_secret: Adapter.config.client_secret,
                grant_type: 'app'
            });
            cache.put(Adapter.config.client_id, token.data.access_token, 1000 * 60 * 58);
            return token.data.access_token;
        } catch (e) {
            Adapter.log.error(e);
        }
    }

    private async GetAllInfoTask() {
        const Adapter = this;

        try {
            if (Adapter.config.vehicle_id) {
                const accessToken = await this.getToken();
                const record = await new tronity.VehiclesApi({
                    accessToken
                }).getManyBaseRecordControllerRecord(Adapter.config.vehicle_id, undefined, undefined, ['createdAt,DESC'], undefined, 1, undefined);
                const recordData = record.data.data;
                if (recordData.length > 0) {
                    Adapter.setState('level', recordData[0].level, true);
                    if (recordData[0].usableLevel)
                        Adapter.setState('usableLevel', recordData[0].usableLevel, true);
                    if (recordData[0].range)
                        Adapter.setState('range', recordData[0].range, true);
                    if (recordData[0].speed)
                        Adapter.setState('speed', recordData[0].speed, true);
                    if (recordData[0].power)
                        Adapter.setState('power', recordData[0].power, true);
                    if (recordData[0].chargerPower)
                        Adapter.setState('chargerPower', recordData[0].chargerPower, true);
                    if (recordData[0].phases)
                        Adapter.setState('phases', recordData[0].phases, true);
                    if (recordData[0].voltage)
                        Adapter.setState('voltage', recordData[0].voltage, true);
                    if (recordData[0].current)
                        Adapter.setState('current', recordData[0].current, true);
                    if (recordData[0].energyAdded)
                        Adapter.setState('energyAdded', recordData[0].energyAdded, true);
                    if (recordData[0].milesAdded)
                        Adapter.setState('milesAdded', recordData[0].milesAdded, true);
                    if (recordData[0].socMax)
                        Adapter.setState('socMax', recordData[0].socMax, true);
                    if (recordData[0].consumption)
                        Adapter.setState('consumption', recordData[0].consumption, true);
                    if (recordData[0].outTemp)
                        Adapter.setState('outTemp', recordData[0].outTemp, true);
                    if (recordData[0].inTemp)
                        Adapter.setState('inTemp', recordData[0].inTemp, true);
                    if (recordData[0].passTemp)
                        Adapter.setState('passTemp', recordData[0].passTemp, true);
                    if (recordData[0].limitSoc)
                        Adapter.setState('limitSoc', recordData[0].limitSoc, true);
                    if (recordData[0].fan)
                        Adapter.setState('fan', recordData[0].fan, true);
                    if (recordData[0].climate)
                        Adapter.setState('climate', recordData[0].climate, true);
                    if (recordData[0].seatHeating)
                        Adapter.setState('seatHeating', recordData[0].seatHeating, true);
                    if (recordData[0].preconditioning)
                        Adapter.setState('preconditioning', recordData[0].preconditioning, true);
                    if (recordData[0].charging)
                        Adapter.setState('charging', recordData[0].charging, true);
                    if (recordData[0].driving)
                        Adapter.setState('driving', recordData[0].driving, true);
                    if (recordData[0].doorLocked)
                        Adapter.setState('doorLocked', recordData[0].doorLocked, true);
                    if (recordData[0].sentryMode)
                        Adapter.setState('sentryMode', recordData[0].sentryMode, true);
                    if (recordData[0].windowsLocked)
                        Adapter.setState('windowsLocked', recordData[0].windowsLocked, true);
                    if (recordData[0].odometer)
                        Adapter.setState('odometer', recordData[0].odometer, true);
                    Adapter.setState('latitude', recordData[0].latitude.toString(), true);
                    Adapter.setState('longitude', recordData[0].longitude.toString(), true);
                }
            }
        } catch (e) {
            Adapter.log.error(e);
        }

        this.GetAllInfoTimeout = setTimeout(() => Adapter.GetAllInfoTask(), 60 * 1000);
    }

    // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
    private async onMessage(msg: any): Promise<void> {
        // eslint-disable-next-line @typescript-eslint/no-this-alias
        const Adapter = this;
        if (msg.command === 'validate') {
            const client_id = msg.message.client_id;
            const client_secret = msg.message.client_secret;

            Adapter.log.info('Try to validate login data and get vehicles');
            try {
                const token = await new tronity.AuthenticationApi().authControllerAuthentication({
                    client_id,
                    client_secret,
                    grant_type: 'app'
                });
                const vehicles = await new tronity.VehiclesApi({
                    accessToken: token.data.access_token
                }).getManyBaseVehicleControllerVehicle();
                Adapter.sendTo(msg.from, msg.command, { success: true, vehicles: vehicles.data.data }, msg.callback);
            } catch (e) {
                Adapter.sendTo(msg.from, msg.command, { success: false }, msg.callback);
            }
        }
    }

    private async onStateChange(id: string, state: ioBroker.State | null | undefined): Promise<void> {
        const Adapter = this;
        if (!state) return;
        Adapter.log.debug(`State Change: ${id} to ${state.val} ack ${state.ack}`);

        const State = await Adapter.getStateAsync('info.connection');
        if (!State || !State.val) {
            Adapter.log.warn('You tried to set a State, but there is currently no valid Token, please configure Adapter first!');
            return;
        }

        const currentId = id.substring(Adapter.namespace.length + 1);
        switch (currentId) {
            case 'command.Charging':
                if (state.val) {
                    try {
                        const accessToken = await this.getToken();
                        await new tronity.VehiclesApi({
                            accessToken
                        }).vehicleControllerChargeStart(Adapter.config.vehicle_id);
                        Adapter.log.info('Try to start charging!');
                    } catch (e) {
                        Adapter.log.error(e);
                    }
                } else {
                    try {
                        const accessToken = await this.getToken();
                        await new tronity.VehiclesApi({
                            accessToken
                        }).vehicleControllerChargeStop(Adapter.config.vehicle_id);
                        Adapter.log.info('Try to stop charging!');
                    } catch (e) {
                        Adapter.log.error(e);
                    }
                }
                break;
        }
    }

    /**
     * Is called when adapter shuts down - callback has to be called under any circumstances!
     */
    private onUnload(callback: () => void): void {
        const Adapter = this;
        try {
            this.log.info('cleaned everything up...');
            if (Adapter.GetAllInfoTimeout) {
                clearTimeout(Adapter.GetAllInfoTimeout);
            }
            callback();
        } catch (e) {
            callback();
        }
    }
}

if (module.parent) {
    // Export the constructor in compact mode
    module.exports = (options: Partial<ioBroker.AdapterOptions> | undefined) => new Tronity(options);
} else {
    // otherwise start the instance directly
    (() => new Tronity())();
}
