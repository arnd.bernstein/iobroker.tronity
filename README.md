![Logo](admin/tronity.png)
# ioBroker.tronity

## tronity adapter for ioBroker

Read vehicle infos from BMW, MINI, Jaguar, Audi, Volkswagen, Skoda, Seat, Kia, Hyundai, Renault, Nissan, Opel, Peugeot and Tesla. This section is intended for the developer. It can be deleted later

## Changelog

### 1.0.0
* (tronity) update npm package

### 0.0.2
* (tronity) update npm package
* (tronity) add support for start and stop
* (tronity) fix issue if no vehicle name is available 

### 0.0.1
* (tronity) initial release

## License
MIT License

Copyright (c) 2020 tronity <info@tronity.io>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.